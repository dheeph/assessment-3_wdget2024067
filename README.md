# Assessment_3-WDGET2024067


### Tech Stack 

- Node 
    - express
    - eslint
- Gitlab 
- Jenkins (with required plugins gitlab,git)
- ngrok

Test the Gitlab connection from Jenkins before starting

### Stages

- Initializing repository
- Checking out to new branch **master**
- Pushing the code files
- Tunnelling IP (ngrok)
- Configuring webhook
- Setting up gitlab pipeline & Jenkinsfile


## Tunneling IP

Since webhook requires a public IP, and jenkins runs in the local machine, you can change the domain for jenkins, if you have one, else
  - Install ngrok 
  - Sign up and get the auth token, run it in the terminal
  - run bash ``` ngrok http http://localhost:8080 ``` in terminal
  This will provide you a public domain, we will use this to create webhook


## Jenkins - reconfigure

- Navigate to Manage jenkins -> System -> Jenkins location.
- Change the jenkins URL to the public domain from ngrok.
- Create a pipeline.

## Webhook Configuration
- ***Jenkins***
  - Under Build triggers -> Enable Build when a change is pushed to GitLab.
  - Under Advanced, generate the secret token.
- ***GitLab***
  - Navigate to repository settings -> webhook -> Add new webhook.
  - Paste the URL and Secret token from jenkins.
  - Enable push events.
  - Test the webhook, it should return ```HTTP 200``` for a successful test.

